#ifndef SCANNER_H__
#define SCANNER_H_

#include <netinet/in.h>

extern int verbose;

int sockfd, connection_status;
struct sockaddr_in sa;

typedef struct {
    int p_start;
    int p_end;
} thread_data_t;

/* check if given ip is valid */
int is_valid(char * host);

/* get ip from hostname */
int resolve(char * host);

/* create socket and set it non-blocking */
int init_socket(struct sockaddr_in * addr, char * ip, int port);

int start_scan(char *addr, int min_port, int max_port, int nthreads);

/* scanner thread function */
void *connect_scan(void *arg);

#endif
