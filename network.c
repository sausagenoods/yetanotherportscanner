#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <pthread.h>
#include "scanner.h"

pthread_mutex_t lock = PTHREAD_MUTEX_INITIALIZER;

void *connect_scan(void *arg) {
    pthread_mutex_lock(&lock);

    thread_data_t *target = (thread_data_t *)arg;

    for (int i = target->p_start; i <= target->p_end; i++) {

        sa.sin_port = htons(i);

        if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
            perror("Failed to create socket\n");
            exit(-1);
        }

        connection_status = connect(sockfd, (struct sockaddr *)&sa, sizeof(sa));

        if (connection_status == -1) {
            printf("%d\tclosed\n", i);
            // TODO: make it timeout.
        } else {
            printf("%d\topen\n", i);
        }
        close(sockfd);
    }
    pthread_mutex_unlock(&lock);
}
