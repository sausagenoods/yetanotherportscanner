#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>         /* getopt */
#include <sys/sysinfo.h>    /* get_nprocs */
#include "scanner.h"

#define MAX_PORT 65535
#define MIN_PORT 1

int verbose = 0;

void print_usage() 
{
    printf("Usage: ./scanner host -p min_port-max_port [-t threads] [-v]\n"
        "\t-p: port range to be specified\n"
        "\t-t: number of threads\n"
        "\t-v: print additional info\n"
        "\t-h: display what you're reading now\n");

    exit(EXIT_FAILURE);
}

int main(int argc, char ** argv) 
{
    char addr[32] = {0};
    int min_port = MIN_PORT;
    int max_port = MAX_PORT;
    int num_of_threads = get_nprocs(); /* num of processors */

    int opt, index;

    while ((opt = getopt(argc, argv, "p:vt:h")) != -1) {
        switch (opt) {
        case 'h':
            print_usage();
            break;
        case 'p':
            if (sscanf(optarg, "%d-%d", & min_port, & max_port) != 2)
                print_usage();
            break;
        case 't':
            if (atoi(optarg) > 10) {
                printf("thread number too high falling back to 10\n");
                num_of_threads = 10;
            } else {
                num_of_threads = atoi(optarg);
            }
            break;
        case 'v':
            verbose = 1;
            break;
        case '?':
            print_usage();
        }
    }

    if (optind < argc)
        strcpy(addr, argv[optind++]);
    else
        print_usage();

    if (!is_valid(addr)) {
        if (!resolve(addr)) {
            printf("invalid addr!\n");
            exit(EXIT_FAILURE);
        }
    }

    start_scan(addr, min_port, max_port, num_of_threads);

    return 0;
}
