#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netdb.h>      /* hostent, gethostbyname */
#include <pthread.h>
#include "scanner.h"

int is_valid(char *host) 
{
    unsigned int d[4];
    char tail[16];
    int c = sscanf(host, "%3u.%3u.%3u.%3u.%s", &d[0], &d[1], &d[2], &d[3], tail);

    if (c != 4 || tail[0])
        return 0;

    for (int i = 0; i < 4; i++)
        if (d[i] > 255)
            return 0;

    return 1;
}

int resolve(char *host) 
{
    struct hostent *he;
    if ((he = gethostbyname(host)) == NULL) {
        if (verbose)
            printf("gethostbyname");
        return 0;
    }

    char addr[100];
    strcpy(addr, inet_ntoa(*(struct in_addr *)he->h_name));

    if (verbose)
        printf("resolved into %s\n", addr);

    return 1;
}

int start_scan(char *addr, int min_port, int max_port, int nthreads) 
{
    // specify an address for the socket
    sa.sin_family = AF_INET;
    sa.sin_addr.s_addr = inet_addr(addr); //set up host

    int total, parts, extra, i;

    total = max_port - min_port;
    parts = total / nthreads;
    extra = total % nthreads;

    thread_data_t *t_data;
    t_data = malloc(sizeof(thread_data_t) * nthreads);

    pthread_t *scan_threads;
    scan_threads = malloc(sizeof(pthread_t *) * nthreads);

    for (i = 0; i < nthreads; i++) {
        t_data[i].p_start = (i == 0) ? min_port : min_port + i * parts + 1;
        t_data[i].p_end = min_port + (i + 1) * parts;

        if (verbose)
            printf("thread: %d\tstart: %d\tend: %d\n", i, t_data[i].p_start, t_data[i].p_end);

        if (pthread_create(&scan_threads[i], NULL, connect_scan, &t_data[i])) {
            printf("error creating thread");
            exit(EXIT_FAILURE);
        }
    }

    if (extra != 0) {
        thread_data_t *extra_data;
        extra_data = malloc(sizeof(thread_data_t));

        pthread_t extra_thread;

        extra_data->p_start = t_data[nthreads - 1].p_end;
        extra_data->p_end = max_port;

        if (verbose)
            printf("extra thread spawned!: start: %d\t end: %d\n", extra_data->p_start, extra_data->p_end);
        pthread_create(&extra_thread, NULL, connect_scan, extra_data);

        pthread_join(extra_thread, NULL);
    }

    for (i = 0; i < nthreads; i++) {
        if (pthread_join(scan_threads[i], NULL)) {
            printf("error joining thread");
            exit(EXIT_FAILURE);
        }
    }

    free(scan_threads);
    free(t_data);

    return 0;
}
