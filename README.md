# YAPS - Yet Another Port Scanner (Old)

A multithreaded port scanner that is capable of doing only TCP connect scans.

No longer maintained.

<h4>Usage:</h4>

```
Usage: ./scanner host -p min_port-max_port [-t threads] [-v]\n"
```

To compile:

```
gcc main.c network.c scanner.c -lpthread -o scanner
```
